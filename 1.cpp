#include "sqlite3.h"
#include <iostream>
using namespace std;
static int callback(void* data, int argc, char **argv, char** azCol);
void main(int argc,char* argv[])
{
	int rc;
	const char* sql;
	sqlite3* db;
	char *zErrMsg = 0;
	//open database
	rc = sqlite3_open("FirstPart.db", &db);
	if (rc)
		cout << "Cannot open database" << sqlite3_errmsg(db) << endl;
	else
		cout << "Opened database successfully" << endl;
	//create table
	sql = "create table people(id integer primary key autoincrement,name string);";
	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK) 
	{
		cout << "SQL error: %s\n\n" << zErrMsg;
		sqlite3_free(zErrMsg);
	}
	else 
	{
		cout << "Table created successfully\n";
	}
	//insert values
	sql = "insert into people(name)"\
		"values('or');"\
		"insert into people(name)"\
		"values('maor');"\
		"insert into people(name)"\
		"values('liron')";
	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: %s\n" << zErrMsg;
		sqlite3_free(zErrMsg);
	}
	else 
	{
		cout << "Records created successfully\n";
	}
	//update value
	sql = "update people set name = 'king' where id = 3";
	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK) 
	{
		cout << "SQL error: %s\n" << zErrMsg;
		sqlite3_free(zErrMsg);
	}
	else
	{
		cout << "Update successfully\n";
		//print values
		cout << "Values in table:" << endl;
		sql = "select * from people;";
		rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
		if (rc != SQLITE_OK) 
		{
			cout << "SQL error: %s\n" << zErrMsg;
			sqlite3_free(zErrMsg);
		}
		else
		{
			cout << "Print successfully\n";
		}
	}
	//end
	sqlite3_close(db);
	system("pause");
}
static int callback(void* data, int argc, char **argv, char** azCol)
{
	for (int i = 0; i < argc; i++)
	{
		cout << azCol[i] << "=" << argv[i] << endl;
	}
	return 0;
}