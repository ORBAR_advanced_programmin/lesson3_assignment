#include "sqlite3.h"
#include <iostream>
#include <string>
#include <stdio.h>
#include <sstream>
using namespace std;
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);
static int callback(void* data, int argc, char **argv, char** azCol);
static int getIdAvailable(void* data, int argc, char **argv, char** azCol);
static int getIdBalance(void* data, int argc, char **argv, char** azCol);
//Global values
int idAvailable;
int idPrice;
int idBalance;
int idToFind;
string idName;
void main()
{
	idAvailable = 0, idPrice = 0, idBalance = 0, idToFind = 0;
	int rc;
	const char* sql;
	sqlite3* db;
	char *zErrMsg = 0;
	//open database
	rc = sqlite3_open("carsDealer.db", &db);
	if (rc)
		cout << "Cannot open database" << sqlite3_errmsg(db) << endl;
	else
		cout << "Opened database successfully" << endl;
	//0 false,1 true
	cout << "Car id 16 and buyer id 3: " << carPurchase(3, 16, db, 0) << endl;//success
	cout << "Car id 12 and buyer id 1: " << carPurchase(1, 12, db, 0) << endl;//success
	cout << "Car id 1 and buyer id 1: " << carPurchase(1, 1, db, 0) << endl;//fail
	cout << "Transaction result from 1 to 2: " << balanceTransfer(1, 2, 50000, db, 0) << endl;//successs
	sqlite3_close(db);
	system("pause");
}
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	const char* cars;
	idToFind = carid;
	cars = "select * from cars;";
	sqlite3_exec(db, cars, getIdAvailable, 0, &zErrMsg);
	idToFind = buyerid;
	cars = "select * from accounts;";
	sqlite3_exec(db, cars, getIdBalance, 0, &zErrMsg);
	cout << "Car available: "<< idAvailable << endl;
	cout << "Car price: "<<idPrice << endl;
	cout << "buyer balance: "<<idBalance << endl;
	if (idAvailable == 1 && idBalance >= idPrice)
		return true;
	return false;
}
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{	
	int rc;
	string sql;
	sql = "select * from accounts;";
	sqlite3_exec(db, sql.c_str(), getIdBalance, 0, &zErrMsg);
	int fromBalance = idBalance;
	idToFind = to;
	sql = "select * from accounts;";
	sqlite3_exec(db, sql.c_str(), getIdBalance, 0, &zErrMsg);
	int toBalance = idBalance;
	cout << "from current balance: " << fromBalance << endl;
	cout << "to current balance: " << toBalance << endl;
	if (fromBalance < amount)
	{
		cout << "Transaction failed" << endl;
		return false;
	}
	else
	{

		sql = "begin transaction;";
		rc = sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);
		if (rc != SQLITE_OK) {
			cout << "SQL error: %s\n\n" << zErrMsg;
			sqlite3_free(zErrMsg);
		}
		sql = "update accounts set balance = " + to_string(fromBalance-amount) + " where id = " + to_string(from);
		rc = sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);
		if (rc != SQLITE_OK) {
			cout << "SQL error: %s\n\n" << zErrMsg;
			sqlite3_free(zErrMsg);
		}
		sql = "update accounts set balance = " + to_string(toBalance + amount) + " where id = " + to_string(to);
		rc = sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);
		if (rc != SQLITE_OK) {
			cout << "SQL error: %s\n\n" << zErrMsg;
			sqlite3_free(zErrMsg);
		}
		else
			cout << "Update successed" << endl;
		sql = "commit;";
		rc = sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);
		if (rc != SQLITE_OK) {
			cout << "SQL error: %s\n\n" << zErrMsg;
			sqlite3_free(zErrMsg);
		}
		else
			cout << "Update successed" << endl;
		cout << "from new balance: " << (fromBalance - amount) << endl;
		cout << "to new balance: " << (toBalance + amount) << endl;
		cout << "Transaction successed" << endl;
		return true;
	}
	
	return true;
}

static int callback(void* data, int argc, char **argv, char** azCol)
{
	for (int i = 0; i < argc; i++)
	{
		cout << azCol[i] << "=" << argv[i] << endl;
	}
	return 0;
}
static int getIdAvailable(void* data, int argc, char **argv, char** azCol)
{
	if (atoi(argv[0]) == idToFind)
	{
		idAvailable = atoi(argv[4]);
		idPrice = atoi(argv[3]);
	}
	return 0;
}
static int getIdBalance(void* data, int argc, char **argv, char** azCol)
{
	if (atoi(argv[0]) == idToFind)
	{
		idBalance = atoi(argv[2]);
	}
	return 0;
}
